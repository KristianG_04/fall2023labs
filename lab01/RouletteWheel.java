import java.util.Random;

public class RouletteWheel{
    
    private Random rand;
    private int lastNum;

    public RouletteWheel(){
        this.rand = new Random();
        this.lastNum = 0;
    }

    public void spin(){
        this.lastNum = rand.nextInt(37);
    }

    public int getValue(){
        return lastNum;
    }
}
