import java.util.Scanner;

public class Roulette {
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);
        RouletteWheel wheel = new RouletteWheel();
        double userMoney = 1000;
        double totalLost = 0;
        double totalWon = 0;
        boolean gameOver = false;

        while (!gameOver){
            System.out.print("Welcome to Roulette. What number would you like to bet on?: ");
            int number = reader.nextInt();

            while (number < 0 || number > 36){
                System.out.print("The number must be between 0 and 36. Try again: ");
                number = reader.nextInt();
            }
            
            System.out.print("You picked " + number + ". How much money would you like to bet?: ");
            double bet = reader.nextDouble();
    
            while (bet > userMoney){
                System.out.print("You do not have enough money to bet. Try again: ");
                bet = reader.nextDouble();
            }
    
            wheel.spin();
    
            if (wheel.getValue() == number){
                System.out.println("You won!");
                userMoney += (35 * bet);
                totalWon += (35 * bet);
            }
            else {
                System.out.println("You lost.");
                userMoney -= bet;
                totalLost += bet;
            }

            System.out.println("Current balance: " + userMoney + "\n");

            System.out.print("Would you like to play again? (y/n): ");
            String answer = reader.next();

            if (answer.equalsIgnoreCase("n")){
                gameOver = true;
            }
        }

        System.out.println("Thank you for playing. \n \nTotal amount won: " + totalWon + "\nTotal amount lost: " + totalLost);
    }
}
