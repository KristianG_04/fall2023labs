//Kristian Garkov - 2237634
package linearalgebra;

public class Vector3d {
    public double x;
    public double y;
    public double z;

    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double magnitude(){
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    public double dotProduct(Vector3d v2){
        return (this.x * v2.getX()) + (this.y * v2.getY()) + (this.z * v2.getZ());
    }

    public Vector3d add(Vector3d v){
        double newX = this.x + v.getX();
        double newY = this.y + v.getY();
        double newZ = this.z + v.getZ();

        Vector3d newVector = new Vector3d(newX, newY, newZ);

        return newVector;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }
}
