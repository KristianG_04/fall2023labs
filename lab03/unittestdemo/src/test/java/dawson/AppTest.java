package dawson;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    @Test
    public void checkEchoMethod(){
        assertEquals("check if echo method works", 5, App.echo(5));
    }

    @Test
    public void checkOneMoreMethod(){
        assertEquals("check if oneMore method works", 6, App.echo(5));
    }
}
