package polymorphism;

public class BookStore {
    public static void main(String[] args){
        Book[] books = new Book[5];

        books[0] = new Book("book 1", "author 1");
        books[1] = new ElectronicBook("eBook 2", "author 2", 10);
        books[2] = new Book("book 3", "author 3");
        books[3] = new ElectronicBook("eBook 4", "author 4", 20);
        books[4] = new ElectronicBook("eBook 5", "author 5", 30);

        for (Book b : books){
            System.out.println(b);
        }

        //1 - The toString of the Book class

        //2 - The toString of the ElectronicBook class

        //3 - books[0].getNumberBytes()

        //4 - books[1].getNumberBytes()

        //5 - ElectronicBook b = books[1];

        //6 - ElectronicBook b = (ElectronicBook)books[1];

        //7 - System.out.println(b.getNumberBytes());

        //8
        //ElectronicBook b = (ElectronicBook) books[0];
        //System.out.println(b.getNumberBytes());
    }
}
