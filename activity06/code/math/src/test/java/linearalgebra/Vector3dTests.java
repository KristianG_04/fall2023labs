//Kristian Garkov - 2237634
package linearalgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for Vector3d.
 */
public class Vector3dTests 
{
    @Test
    public void checkGetters(){
        Vector3d v = new Vector3d(3, 5, 7);

        assertEquals(3, v.getX(), 0);
        assertEquals(5, v.getY(), 0);
        assertEquals(7, v.getZ(), 0);
    }

    @Test
    public void checkMagnitude(){
        Vector3d v = new Vector3d(5, 6, 3);

        assertEquals(Math.sqrt(70), v.magnitude(), 0);
    }

    @Test
    public void checkDotProduct(){
        Vector3d v1 = new Vector3d(5, 3, 8);
        Vector3d v2 = new Vector3d(2, 9, 10);

        assertEquals(117, v1.dotProduct(v2), 0);
    }

    @Test
    public void checkAdd(){
        Vector3d v1 = new Vector3d(5, 3, 7);
        Vector3d v2 = new Vector3d(8, 6, 4);
        Vector3d v3 = v1.add(v2);

        assertEquals(13, v3.getX(), 0);
        assertEquals(9, v3.getY(), 0);
        assertEquals(11, v3.getZ(), 0);
    }
}
