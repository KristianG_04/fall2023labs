//Kristian Garkov - 2237634
package linearalgebra;

/**
 * Vector3d is class used to create vectors and perform operations on them.
 * @author Kristian Garkov
 * @version 10/2/2023
 */
public class Vector3d {
    public double x;
    public double y;
    public double z;

    /**
     * Constructor for the Vector3d object.
     * @param x Value of x.
     * @param y Value of y.
     * @param z Value of z.
     */
    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Calculates the magnitude of the vector.
     * @return The square root of the sum of the squared values of x, y and z. 
     */
    public double magnitude(){
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
    }

    /**
     * Calculates the dot product of 2 vectors.
     * @param v2 Second vector.
     * @return The dot product of both vectors.
     */
    public double dotProduct(Vector3d v2){
        return (this.x * v2.getX()) + (this.y * v2.getY()) + (this.z * v2.getZ());
    }

    /**
     * Adds 2 vectors together.
     * @param v Second vector.
     * @return The sum of both vectors.
     */
    public Vector3d add(Vector3d v){
        double newX = this.x + v.getX();
        double newY = this.y + v.getY();
        double newZ = this.z + v.getZ();

        Vector3d newVector = new Vector3d(newX, newY, newZ);

        return newVector;
    }

    /**
     * Get method for the x field.
     * @return Value of x.
     */
    public double getX(){
        return this.x;
    }

    /**
     * Get method for the y field.
     * @return Value of y.
     */
    public double getY(){
        return this.y;
    }

    /**
     * Get method for the z field.
     * @return Value of z.
     */
    public double getZ(){
        return this.z;
    }
}
