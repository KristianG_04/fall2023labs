package geometry;

public class LotsOfShapes {
    public static void main(String[] args) {
        Shape[] shapes = new Shape[5];

        shapes[0] = new Circle(5);
        shapes[1] = new Rectangle(7, 13);
        shapes[2] = new Square(10);
        shapes[3] = new Rectangle(9, 15);
        shapes[4] = new Circle(14);

        for(Shape s : shapes) {
            System.out.println("Area: " + s.getArea() + " Perimeter: " + s.getPerimeter());
        }
    }
}
