//Part 2

import java.util.ArrayList;

public class ListPractice2 {
    public static void main(String[] args) {
        String[] s = {"HELLO", "HI", "EEeE", "RE"};

        ArrayList<String> words = new ArrayList<String>();
        System.out.println(words.size());

        for (String st : s) {
            words.add(st);
        }
        
        System.out.println(words.size());

        System.out.println(countUpperCase(words));
        System.out.println(getUpperCase(words));

        //ArrayList<String> stillEmpty = new ArrayList<String>(50);
        //System.out.println(stillEmpty.size());
    }

    public static int countUpperCase(String[] strings) {
        int count = 0;

        for (String s : strings) {
            if (isUpperCase(s)) {
                count++;
            }
        }

        return count;
    }

    public static int countUpperCase(ArrayList<String> strings) {
        int count = 0;

        for (String s : strings) {
            if (isUpperCase(s)) {
                count++;
            }
        }

        return count;
    }

    public static boolean isUpperCase(String s) {
        return s.matches("[A-Z]+");
    }

    public static String[] getUpperCase(String[] strings) {
        String[] onlyUpper = new String[countUpperCase(strings)];

        int count = 0;

        for (int i = 0; i < strings.length; i++) {
            if (isUpperCase(strings[i])) {
                onlyUpper[count] = strings[i];
                count++;
            }
        }

        return onlyUpper;
    }

    public static ArrayList<String> getUpperCase(ArrayList<String> strings) {
        ArrayList<String> newStrings = new ArrayList<String>();

        for (String s : strings) {
            if (isUpperCase(s)) {
                newStrings.add(s);
            }
        }

        return newStrings;
    }
}