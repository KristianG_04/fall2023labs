//Part 4

import java.util.ArrayList;
import java.util.List;

public class ListPractice4 {
    public static void main(String[] args) {
        String[] s = {"HELLO", "HI", "EEeE", "RE"};

        List<String> words = new ArrayList<String>();
        System.out.println(words.size());

        for (String st : s) {
            words.add(st);
        }

        System.out.println(words.contains("HELLO"));
        System.out.println(words.contains("hi"));
        
        //System.out.println(words.size());

        //System.out.println(countUpperCase(words));
        //System.out.println(getUpperCase(words));

        //List<String> stillEmpty = new ArrayList<String>(50);
        //System.out.println(stillEmpty.size());
    }

    public static int countUpperCase(String[] strings) {
        int count = 0;

        for (String s : strings) {
            if (isUpperCase(s)) {
                count++;
            }
        }

        return count;
    }

    public static int countUpperCase(List<String> strings) {
        int count = 0;

        for (String s : strings) {
            if (isUpperCase(s)) {
                count++;
            }
        }

        return count;
    }

    public static boolean isUpperCase(String s) {
        return s.matches("[A-Z]+");
    }

    public static String[] getUpperCase(String[] strings) {
        String[] onlyUpper = new String[countUpperCase(strings)];

        int count = 0;

        for (int i = 0; i < strings.length; i++) {
            if (isUpperCase(strings[i])) {
                onlyUpper[count] = strings[i];
                count++;
            }
        }

        return onlyUpper;
    }

    public static List<String> getUpperCase(List<String> strings) {
        List<String> newStrings = new ArrayList<String>();

        for (String s : strings) {
            if (isUpperCase(s)) {
                newStrings.add(s);
            }
        }

        return newStrings;
    }
}