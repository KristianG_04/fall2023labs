//Part 1

public class ListPractice {
    public static void main(String[] args) {
        String[] s = {"HELLO", "HI", "EEeE", "RE"};
        System.out.println(countUpperCase(s));

        String[] onlyUpper = getUpperCase(s);

        for (String st : onlyUpper) {
            System.out.println(st);
        }
    }

    public static int countUpperCase(String[] strings) {
        int count = 0;

        for (String s : strings) {
            if (isUpperCase(s)) {
                count++;
            }
        }

        return count;
    }

    public static boolean isUpperCase(String s) {
        return s.matches("[A-Z]+");
    }

    public static String[] getUpperCase(String[] strings) {
        String[] onlyUpper = new String[countUpperCase(strings)];

        int count = 0;

        for (int i = 0; i < strings.length; i++) {
            if (isUpperCase(strings[i])) {
                onlyUpper[count] = strings[i];
                count++;
            }
        }

        return onlyUpper;
    }
}