package ListPractice5;

import java.util.ArrayList;

public class App {
    public static void main(String[] args) {
        ArrayList<Point> points = new ArrayList<Point>();

        Point target = new Point(5, 5);
        Point target2 = new Point(10, 10);

        points.add(new Point(5, 5));
        points.add(new Point(10, 3));
        points.add(new Point(1, 1));

        System.out.println(points.contains(target));
        System.out.println(points.contains(target2));
    }
}
