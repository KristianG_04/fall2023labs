package ListPractice5;

public class Point {
    public double x;
    public double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Point)) {
            return false;
        }

        Point otherPoint = (Point) other;

        return this.x == otherPoint.x && this.y == otherPoint.y;
    }
}
