//Kristian Garkov - 2237634
package application;

import vehicles.Bicycle;

public class BikeStore {
    public static void main(String[] args){
        Bicycle[] bikes = new Bicycle[4];

        bikes[0] = new Bicycle("bike1", 5, 10);
        bikes[1] = new Bicycle("bike2", 10, 20);
        bikes[2] = new Bicycle("bike3", 15, 30);
        bikes[3] = new Bicycle("bike4", 20, 40);

        for (Bicycle b : bikes){
            System.out.println(b);
        }
    }
}
