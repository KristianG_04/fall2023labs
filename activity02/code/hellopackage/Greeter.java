package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;

public class Greeter {
    public static void main(String[] args){
        Scanner reader = new Scanner(System.in);
        Random rand = new Random();

        System.out.print("Enter an integer: ");
        int num = reader.nextInt();

        System.out.println(Utilities.doubleMe(num));
    }
}